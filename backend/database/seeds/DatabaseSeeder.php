<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Post::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 20; $i++) {
            \App\Post::create([
                'title' => $faker->sentence,
                'body' => $faker->paragraph,
                'author' => $faker->firstName,
                'image' => $faker->imageUrl()
            ]);
        }
    }
}
