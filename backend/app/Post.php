<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Post extends Model
{
    protected $fillable = [
        'title',
        'body',
        'author',
        'image',
        'can_comment'
    ];

    protected $casts = [
        'can_comment' => 'boolean'
    ];

    public function comments() {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    public static function getPostById($id) {
        return self::find($id);
    }

    public static function addPost($data)
    {
        return self::create($data);
    }
}
