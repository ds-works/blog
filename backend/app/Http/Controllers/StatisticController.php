<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
    public function index()
    {
        $posts = Post::count();
        $comments = Comment::count();
        return response()->json(['posts' => $posts, 'comments' => $comments], 200);
    }
}
