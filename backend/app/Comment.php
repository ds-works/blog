<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;

class Comment extends Model
{
    protected $fillable = ['author', 'text', 'parent_id', 'is_answer'];

    protected $casts = [
        'is_answer' => 'boolean',
        'parent_id' => 'integer'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class, 'parent_id');
    }

    public function answers()
    {
        return $this->hasMany(Comment::class, 'parent_id')->where('is_answer', true);
    }

    public static function addComment($data)
    {
        return self::create($data);
    }

    public static function editComment($comment, $data)
    {
        $comment->update($data);
        return $data;
    }
}
