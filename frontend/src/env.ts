const config = {
    type: 'http',
    host: 'localhost',
    port: '8000'
}

export default config;