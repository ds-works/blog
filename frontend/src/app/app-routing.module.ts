import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostComponent } from './post/post.component';
import { StatisticComponent } from './statistic/statistic.component';
import { DetailsComponent } from './post/details/details.component';

const routes: Routes = [
  { path: 'blog', component: PostComponent },
  { path: 'post/:id', component: DetailsComponent },
  { path: 'statistic', component: StatisticComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
