import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import config from '../../env';

@Injectable()
export class StatisticService {
  private server = `${config.type}://${config.host}${
    config.port ? `:${config.port}` : ''
  }/api`;
  constructor(private http: HttpClient) {}

  private _request(method, url, data?) {
    return this.http[method](`${this.server}${url}`, data || {});
  }

  public getStatistic() {
    return this._request('get', '/statistic');
  }
}
