import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import config from '../../env';

@Injectable()
export class CommentService {
  private server = `${config.type}://${config.host}${
    config.port ? `:${config.port}` : ''
  }/api`;
  constructor(private http: HttpClient) {}

  private _request(method, url, data?) {
    return this.http[method](`${this.server}${url}`, data || {});
  }

  public getComments(postId) {
    return this._request('get', `/comments?id=${+postId}`);
  }

  public getComment(id) {
    return this._request('get', `/comments/${id}`);
  }

  public addComment(comment, postId) {
    if (!comment.is_answer) comment.parent_id = +postId;
    return this._request('post', `/comments/`, { comment });
  }

  public editComment(comment) {
    return this._request('patch', `/comments/${comment.id}`, { comment });
  }

  public deleteComment(commentId) {
    return this._request('delete', `/comments/${commentId}`);
  }
}
