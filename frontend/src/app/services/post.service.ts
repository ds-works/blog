import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import config from '../../env';

@Injectable()
export class PostService {
  private server = `${config.type}://${config.host}${
    config.port ? `:${config.port}` : ''
  }/api`;
  constructor(private http: HttpClient) {}

  private _request(method, url, data?) {
    return this.http[method](`${this.server}${url}`, data || {});
  }

  public getPosts() {
    return this._request('get', '/posts');
  }

  public getPost(id) {
    return this._request('get', `/posts/${id}`);
  }

  public addPost(post) {
    return this._request('post', `/posts`, { post });
  }

  public setCanComment(canComment, postId) {
    return this._request('patch', `/posts/${postId}`, { can_comment: canComment });
  }
}
