import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { AppComponent } from './app.component';
import { CommentDialogComponent } from './post/dialog/comment-dialog.component';
import { DetailsComponent } from './post/details/details.component';
import { PostComponent } from './post/post.component';
import { PostDialogComponent } from './post/dialog/post-dialog.component';
import { StatisticComponent } from './statistic/statistic.component';

import { PostService } from './services/post.service';
import { CommentService } from './services/comment.service';
import { StatisticService } from './services/statistic.service';

@NgModule({
  declarations: [
    AppComponent,
    CommentDialogComponent,
    DetailsComponent,
    PostComponent,
    PostDialogComponent,
    StatisticComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatTabsModule,
    ReactiveFormsModule,
    SweetAlert2Module.forRoot(),
  ],
  providers: [PostService, CommentService, StatisticService],
  bootstrap: [AppComponent],
  entryComponents: [CommentDialogComponent, PostDialogComponent],
})
export class AppModule {}
