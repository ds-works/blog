import { Component, OnInit } from '@angular/core';
import { StatisticService } from '../services/statistic.service';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css'],
})
export class StatisticComponent implements OnInit {
  displayedColumns = ['posts', 'comments'];
  statistic;

  constructor(public statisticService: StatisticService) {}

  ngOnInit() {
    this.statisticService.getStatistic().subscribe(result => {
      this.statistic = [result];
    });
  }
}
