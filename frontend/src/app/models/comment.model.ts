export default interface Comment {
  id?: number | string;
  author: string;
  text: string;
  parent_id?: number | string;
  created_at?: Date | string;
  updated_at?: Date | string;
  is_answer?: boolean;
  answers?: Comment[];
}
