export default interface Post {
  id: number;
  title: string;
  body: string;
  image: string;
  can_comment?: boolean;
  author: string;
  created_at: Date;
}
