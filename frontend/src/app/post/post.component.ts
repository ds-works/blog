import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';
import Post from '../models/post.model';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { PostDialogComponent } from './dialog/post-dialog.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
})
export class PostComponent implements OnInit {
  public posts: Post[] = [];
  constructor(private postService: PostService, public dialog: MatDialog) {}

  ngOnInit() {
    this.postService.getPosts().subscribe(posts => {
      this.posts = posts;
    });
  }

  openPostForm(post?) {
    post = post || {};
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      author: post.author || '',
      title: post.title || '',
      body: post.body || '',
      image: post.image || ''
    };

    const dialogRef = this.dialog.open(PostDialogComponent, dialogConfig);

    const subscription = dialogRef.afterClosed().subscribe(data => {
      if (!data) {
        subscription.unsubscribe();
        return;
      }
      this.addPost(data);
      subscription.unsubscribe();
    });
  }

  addPost({ value }) {
    this.postService.addPost(value)
      .subscribe(result => {
        this.posts.unshift(result);
      });
  }
}
