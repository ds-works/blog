import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-comment-dialog',
  templateUrl: './comment-dialog.component.html',
})
export class CommentDialogComponent implements OnInit {
  commentForm: FormGroup;
  comment;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<FormGroup>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.comment = data;
    this.comment.parent_id =
      this.comment.is_answer && !this.comment.edit
        ? this.comment.id
        : this.comment.parent_id;
  }

  ngOnInit() {
    this.commentForm = this.getCommentForm(this.comment);
  }

  save(commentForm) {
    this.dialogRef.close(commentForm);
  }

  close() {
    this.commentForm = this.getCommentForm();
    this.dialogRef.close();
  }

  getCommentForm(comment?) {
    comment = comment || {};
    return this.fb.group({
      id: [!comment.is_answer || comment.edit ? comment.id || '' : ''],
      author: [
        !comment.is_answer || comment.edit ? comment.author || '' : '',
        [Validators.required],
      ],
      text: [
        !comment.is_answer || comment.edit ? comment.text || '' : '',
        [Validators.required, Validators.minLength(3)],
      ],
      parent_id: [comment.parent_id || ''],
      created_at: [
        !comment.is_answer || comment.edit ? comment.created_at || '' : '',
      ],
      updated_at: [
        !comment.is_answer || comment.edit ? comment.updated_at || '' : '',
      ],
      is_answer: [comment.is_answer || false],
      answers: [
        !comment.is_answer || comment.edit ? comment.answers || [] : [],
      ],
    });
  }
}
