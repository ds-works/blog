import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng5-validation';

@Component({
  selector: 'app-post-dialog',
  templateUrl: './post-dialog.component.html',
})
export class PostDialogComponent implements OnInit {
  postForm: FormGroup;
  post;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<FormGroup>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.post = data;
  }

  ngOnInit() {
    this.postForm = this.getPostForm(this.post);
  }

  save(postForm) {
    this.dialogRef.close(postForm);
  }

  close() {
    this.postForm = this.getPostForm();
    this.dialogRef.close();
  }

  getPostForm(post?) {
    post = post || {};
    return this.fb.group({
      author: [post.author || '', [Validators.required, Validators.minLength(3)]],
      title: [post.title || '', [Validators.required, Validators.minLength(3)]],
      body: [post.body || '', [Validators.required, Validators.minLength(3)]],
      image: [post.image || '', [CustomValidators.url]]
    });
  }
}
