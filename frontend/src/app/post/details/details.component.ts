import { Component, OnInit } from '@angular/core';
import { PostService } from '../../services/post.service';
import Post from '../../models/post.model';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { CommentDialogComponent } from '../dialog/comment-dialog.component';
import Comment from '../../models/comment.model';
import { CommentService } from '../../services/comment.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent implements OnInit {
  private id: number;
  public post: Post;
  public comments: Comment[] = [];

  constructor(
    private postService: PostService,
    private commentService: CommentService,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.postService
        .getPost(this.id)
        .subscribe(result => (this.post = result));
      this.commentService.getComments(this.id).subscribe(result => {
        this.comments = result;
      });
    });
  }

  openCommentForm(comment?, edit?: boolean, is_answer?: boolean) {
    comment = comment || {};
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      id: comment.id || '',
      author: comment.author || '',
      text: comment.text || '',
      parent_id: comment.parent_id || '',
      created_at: comment.created_at || '',
      updated_at: comment.updated_at || '',
      is_answer: is_answer || false,
      answers: comment.answers || [],
      edit: edit || false,
    };

    const dialogRef = this.dialog.open(CommentDialogComponent, dialogConfig);

    const subscription = dialogRef.afterClosed().subscribe(data => {
      if (!data) {
        subscription.unsubscribe();
        return;
      }
      if (comment && edit) {
        this.editComment(data);
        subscription.unsubscribe();
        return;
      }
      this.addComment(data);
      subscription.unsubscribe();
    });
  }

  addComment({ value }: { value: Comment }) {
    this.commentService.addComment(value, this.id).subscribe(result => {
      if (result.is_answer) {
        const index = this.comments.findIndex(
          item => item.id === result.parent_id
        );
        if (!this.comments[index].answers) this.comments[index].answers = [];
        this.comments[index].answers.push(result);
        return;
      }
      this.comments.push(result);
    });
  }

  editComment({ value }: { value: Comment }) {
    this.commentService.editComment(value).subscribe(result => {
      if (!result.is_answer) {
        const index = this.comments.findIndex(item => item.id === value.id);
        this.comments.splice(index, 1, result);
      } else {
        const index = this.comments.findIndex(
          item => item.id === value.parent_id
        );
        const answerIndex = this.comments[index].answers.findIndex(
          item => item.id === value.id
        );
        this.comments[index].answers.splice(answerIndex, 1, result);
      }
    });
  }

  deleteComment(comment) {
    this.commentService.deleteComment(comment.id).subscribe(() => {
      if (!comment.is_answer) {
        const index = this.comments.findIndex(item => item.id === comment.id);
        this.comments.splice(index, 1);
      } else {
        const index = this.comments.findIndex(
          item => item.id === comment.parent_id
        );
        const answerIndex = this.comments[index].answers.findIndex(
          item => item.id === comment.id
        );
        this.comments[index].answers.splice(answerIndex, 1);
      }
    });
  }

  hideComments(checked) {
    console.log(checked);
    this.postService
      .setCanComment(!checked, this.post.id)
      .subscribe(() => (this.post.can_comment = !checked));
  }
}
